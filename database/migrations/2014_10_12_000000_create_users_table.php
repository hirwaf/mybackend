<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {            
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('telphone')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('role')->default(1);
            $table->timestamp('last_login')->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('online')->default('0');
            $table->rememberToken()->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
