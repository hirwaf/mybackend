<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('users')->insert([
            'username'  =>  'hirwaf',
            'telphone'  =>  '0789812404',
            'email'     =>  'hirwa.felix@gmail.com',
            'password'  =>  bcrypt('12345'),
            'role'      =>  '7',
        ]);
    }
}
