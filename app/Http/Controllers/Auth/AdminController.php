<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(["auth","admin"], ['except' => 'logout']);
    }
    
    public function dashboard(){
        return view('auth.layouts.dashboard');
    }
}
